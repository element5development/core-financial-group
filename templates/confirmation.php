<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main id="main-content">
	<?php if ( get_field('message') ) : ?>
		<article>
			<section class="is-narrow aligncenter">
				<h1><?php the_title(); ?></h1>
				<p><?php the_field('message'); ?></p>
			</section>
		</article>
	<?php else : ?>
		<article>
			<section>
				<h2>Coming Soon</h2>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>