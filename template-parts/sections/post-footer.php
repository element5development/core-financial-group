<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div class="copyright standard aligncenter">
		<a href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/core-financial-logo.png" alt="Core Financial Group Logo" />
		</a>
		<p>The contents of this webpage are Copyright &#169; <?php echo date('Y'); ?> Core Financial Group. All Rights Reserved.</p>
		<p>Aaron Thierry is a Registered Representative offering Securities through The O.N. Equity Sales Company, Member <a href="https://www.finra.org/" target="_blank">FINRA</a>/<a href="https://www.sipc.org/" target="_blank">SIPC</a>. 39395 West 12 Mile Road, Suite 102, Farmington Hills, MI 48331. 888.466.5453. Aaron Thierry offers Investment Advisory Services through O.N. Investment Management Company.</p>
		<p>Check the background of this firm on <a href="https://brokercheck.finra.org/" target="_blank">FINRA's BrokerCheck</a>.</p>
	</div>
	<div id="element5-credit">
		<a target="_blank" href="https://element5digital.com" rel="nofollow">
			<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit_white.svg" alt="Crafted by Element5 Digital" />
		</a>
	</div>
</footer>