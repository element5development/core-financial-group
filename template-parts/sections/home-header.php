<?php 
/*----------------------------------------------------------------*\

	HOME HEADER

\*----------------------------------------------------------------*/
?>
<?php $image = get_field('background_image'); ?>
<header class="post-head home-head lazyload blur-up" data-expand="500" data-bgset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w" data-sizes="auto">
	<div>
	<?php if ( get_field('title') ) : ?>
		<h1><?php the_field('title'); ?></h1>
	<?php else : ?>
		<h1><?php the_title(); ?></h1>
	<?php endif; ?>

	<?php if ( get_field('sub_title') ) : ?>
		<h2><?php the_field('sub_title'); ?></h2>
	<?php endif; ?>

	<?php
		$link = get_field('button'); 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self'; 
		if ( get_field('button') ) : 
	?>
		<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
	<?php endif; ?>
	</div>
</header>