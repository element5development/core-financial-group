<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>
<?php $image = get_field('background_image'); ?>
<header class="post-head lazyload blur-up" data-expand="500" data-bgset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w" data-sizes="auto">
	<h1><?php the_title(); ?></h1>
</header>