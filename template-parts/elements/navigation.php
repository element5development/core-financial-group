<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<nav>
		<a href="<?php echo get_home_url(); ?>" title="Core Financial Group Home" aria-label="Core Financial Group Home">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/core-financial-logo.png" alt="Core Financial Group Logo" />
		</a>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
		<button class="activate-menu" type="button" value="open-close-mobile-menu" title=“open-close-mobile-menu” aria-label="Open and Close Mobile Menu">
			<div class="hamburger-menu"></div>
		</button>
	</nav>
</div>