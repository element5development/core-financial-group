<?php 
/*----------------------------------------------------------------*\

	FRONT PAGE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/home-header'); ?>

<main id="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php 
			/*----------------------------------------------------------------*\
			|
			| Insert page content which is most often handled via ACF Pro
			| and highly recommend the use of the flexiable content so
			|	we already placed that code here.
			|
			| https://www.advancedcustomfields.com/resources/flexible-content/
			|
			\*----------------------------------------------------------------*/
			?>
			<?php
				$id = 0;
				while ( have_rows('article') ) : the_row();
					$id++;
					if( get_row_layout() == 'editor' ):
						hm_get_template_part( 'template-parts/sections/article/editor', [ 'sectionId' => $id ] );
					elseif( get_row_layout() == '2editor' ):
						hm_get_template_part( 'template-parts/sections/article/editor-2-column', [ 'sectionId' => $id ] );
					elseif( get_row_layout() == '3editor' ):
						hm_get_template_part( 'template-parts/sections/article/editor-3-column', [ 'sectionId' => $id ] );
					elseif( get_row_layout() == 'media+text' ):
						hm_get_template_part( 'template-parts/sections/article/media-text', [ 'sectionId' => $id ] );
					elseif( get_row_layout() == 'cover' ):
						hm_get_template_part( 'template-parts/sections/article/cover', [ 'sectionId' => $id ] );
					elseif( get_row_layout() == 'gallery' ):
						hm_get_template_part( 'template-parts/sections/article/gallery', [ 'sectionId' => $id ] );
					elseif( get_row_layout() == 'card_grid' ):
						hm_get_template_part( 'template-parts/sections/article/card-grid', [ 'sectionId' => $id ] );
					endif;
				endwhile;
			?>
		</article>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<h2>Coming Soon</h2>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>